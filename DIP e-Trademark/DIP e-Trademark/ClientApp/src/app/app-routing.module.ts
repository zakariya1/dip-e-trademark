import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NavComponent } from './shared/nav/nav.component';
import { AllWorklistComponent } from './pages/worklist/worklist.component';

const routes: Routes = [
  { path: '', redirectTo: '/worklist', pathMatch: 'full'},
  { path: 'worklist', component: AllWorklistComponent },
  { path: 'nav', component: NavComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

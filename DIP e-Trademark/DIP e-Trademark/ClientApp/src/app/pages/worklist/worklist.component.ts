import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-worklist',
  templateUrl: './worklist.component.html',
  styleUrls: ['./worklist.component.css']
})
export class AllWorklistComponent implements OnInit {

  public allList: any[];
  public requestList: any[];
  public saveformList: any[];
  
  constructor() { }

  ngOnInit() {

    this.myTaskList1()
    this.myTaskList2()
    this.myTaskList3()

  }

  myTaskList1() {
    this.allList = [
      {
        title: 'กล่องงานรวม',
        description: 'Detail detaildetail',
        icon : 'search',    
      },
    ];
  }

  myTaskList2() {
    this.requestList = [
      {
        title: 'รับคำขอข้อมูล ก.อื่นๆ',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
      {
        title: 'รับคำขอข้อมูล ก.อื่นๆ (พาณิชย์จังหวัด)',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
    ];
  }

  myTaskList3() {
    this.saveformList = [
      {
        title: 'คำขออุทธรณ์ (ก.03)',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
      {
        title: 'คำขออุทธรณ์คัดค้าน (ก.03)',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
      {
        title: 'คำขอให้เพิกถอนการจดทะเบียน (ก.08)',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
      {
        title: 'สแกนเอกสาร',
        description: 'Detail detaildetail',
        icon : 'play_arrow',
      },
    ];


  }

}

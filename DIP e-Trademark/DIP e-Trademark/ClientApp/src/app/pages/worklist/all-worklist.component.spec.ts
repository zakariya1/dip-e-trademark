import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllWorklistComponent } from './worklist.component';

describe('AllWorklistComponent', () => {
  let component: AllWorklistComponent;
  let fixture: ComponentFixture<AllWorklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllWorklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllWorklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
